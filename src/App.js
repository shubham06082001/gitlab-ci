import React from "react"
import "./App.css"
import Counter from "./components/Counter"

function App() {
  return (
    <>
        <h3
        >
          Learn GitLab CI
        </h3>
        <Counter />
    </>
  )
}

export default App
